<?php
function get_google($key){
    $url = 'http://spreadsheets.google.com/feeds/cells/' . $key . '/1/public/values';

    // initialize curl
    $curl = curl_init();

    // set curl options
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

    // get the spreadsheet using curl
    $google_sheet = curl_exec($curl);

    // close the curl connection
    curl_close($curl);

    // import the xml file into a SimpleXML object
    $feed = new SimpleXMLElement($google_sheet);


    $array = array();
    foreach ($feed->entry as $entry) {
        $location = (string) $entry->title;
        preg_match('/(?P<column>[A-Z]+)(?P<row>[0-9]+)/', $location, $matches);
        $array[$matches['row']][$matches['column']] = (string) $entry->content;
    }

    $result = array();
    $header = true;
    $array_header = array();
    foreach ($array as $list) {
        if($header)
        {
            // Set Header
            $header = false;
            $array_header = $list;
        }
        else
        {
            // Fill Content

           /* $temp = array();
            foreach($array_header as $key => $value)
            {
                $temp[$value] = $list[$key];
            }
            $result[] = $temp;*/

            $result[] = $list;
        }
    }

    $return = array(
        'header' => $array_header,
        'data' => $result,
        'date' => date('Y-m-d')
    );
    // return the array
    return $return;
}
$KEY = "1yHn_UTs_pl35CZZSUi3SLZSplnwToF_WAVfHRd91r3M";
$data = get_google($KEY);

echo "<pre>";
print_r($data);
echo "</pre>";
//echo json_encode($data);
