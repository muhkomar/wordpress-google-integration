<?php echo $this->css('framepress.default.css');?>

<style>
    label {padding-bottom: 10px; display: block}
    .wrap {padding: 30px;}
    .wrap p  {font-size:14px;}
    .wrap pre { background: #2E3436; color:#fff; padding: 15px; border-radius: 5px; font-size: 16px; line-height: 15px;}
    .wrap pre b {color: #90CAEE; font-weight: bold; font-size:20px;}
    .wrap input[type="text"], .wrap textarea { width: 500px; margin-bottom: 30px; box-shadow: 0 0 15px #999; border-width:2px; border-color: #f0f0f0; margin-right: 20px; }
</style>

<div class="wrap">
    <h2>Upwardstech Google Spreadsheets</h2><br>
    <?php echo $this->form(array('function'=> 'setkey'));?>
    <label>Google Spreadsheet Key :</label>
    <input type="text" name="Gkey" placeholder="Google Spreadsheet Key" value="<?php echo get_option('Gkey') ?>">
    <input type="submit" value="Save">
    </form>
    <?php
    $Gkey = get_option('Gkey');
    if (!empty($Gkey) ){
     ?>
        <?php echo $this->form(array('function'=> 'get_spreadsheets'));?>
        <input type="submit" value="Update Google Spreadsheets">
        </form>
    <?php
    }
    ?>
    <?php
    $date = get_option('Gspreadsheets');
    if (!empty($date)){
        echo '<p>Last Update : ';
        echo $date['date'];
        echo '</p>';
    }
    ?>
    <?php
    $Gspreadsheets = get_option('Gspreadsheets');
    $header = $Gspreadsheets['header'] ;
    $data = $Gspreadsheets['data'];

    ?>
    <table>
        <thead>
        <tr>
            <th>School Name</th>
            <th>Slug</th>
        </tr>
        </thead>
        <tbody>

            <?php
            foreach ($data as $key => $val)
            {
                echo ' <tr> <td>';
                echo $val['B'];
                echo '</td>';
                echo '<td>';
                echo sanitize_title ($val['B']);
                echo '</td></tr>';
            }
            ?>
        </tbody>
    </table>
</div>
