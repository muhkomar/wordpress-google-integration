<?php

class UtSpreadsheetMain
{

	/*
	 * Handler for  FramePress menu link
	 *
	 * Cause this is a page, this function will render
	 * its view automatically before the function finish
	*/


    public function home(){
        global $utSpreadsheet;
        $utSpreadsheet->viewSet();
    }

    public function setkey(){
        global $utSpreadsheet;
        if ($_POST){
            $key = $_POST['Gkey'];
            update_option('Gkey',$key);

        }
        $utSpreadsheet->redirect(array('function'=>'home'));
    }

    function get_spreadsheets(){
        global $utSpreadsheet;

        $key = get_option('Gkey');
        $url = 'http://spreadsheets.google.com/feeds/cells/' . $key . '/1/public/values';

        // initialize curl
        $curl = curl_init();

        // set curl options
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

        // get the spreadsheet using curl
        $google_sheet = curl_exec($curl);

        // close the curl connection
        curl_close($curl);

        // import the xml file into a SimpleXML object
        $feed = new SimpleXMLElement($google_sheet);


        $array = array();
        foreach ($feed->entry as $entry) {
            $location = (string) $entry->title;
            preg_match('/(?P<column>[A-Z]+)(?P<row>[0-9]+)/', $location, $matches);
            $array[$matches['row']][$matches['column']] = (string) $entry->content;
        }

        $result = array();
        $header = true;
        $array_header = array();
        foreach ($array as $list) {
            if($header)
            {
                // Set Header
                $header = false;
                $array_header = $list;
            }
            else
            {
                // Fill Content

               /* $temp = array();
                foreach($array_header as $key => $value)
                {
                    $temp[$value] = $list[$key];
                }
                $result[] = $temp;*/

                $slug = sanitize_title ($list['B']);
                $result[$slug] = $list;

            }
        }

        $return = array(
            'header' => $array_header,
            'data' => $result,
            'date' => date('d-m-Y h:i A')
        );

        update_option('Gspreadsheets',$return);
        $utSpreadsheet->redirect(array('function'=>'home'));

    }


}


?>
