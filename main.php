<?php
    /**
     * Plugin Name: Upwardstech Google spreadsheets
     * Description: Plugin to pull in and display spreadsheets from Google docs
     * Author: Upwardstech
     * Author URI: http://upwardstech.com
     * Version: 1.1
     **/

	//init framework
	require_once( 'core/FPL.php' );
	global $FramePress;

	/**
	*	Create your global instance of framepress, and configure it
	*	see "Creating and configuring your instance of framepress" documentation
	* 	--------------------------------------------------------------------------------------------------------------------
	*/
	global $utSpreadsheet;
    $utSpreadsheet = new $FramePress(__FILE__, array(
		'prefix' => 'utSpreadsheet',
		'debug' => true,
	));


	/**
	*	Modifing / Adding paths
	*	see "Adding custom Paths" documentation
	* 	--------------------------------------------------------------------------------------------------------------------
	*/
/*
	$test->mergePaths(array(
		'superlibs' => $test->path['lib'] . DS . 'super';
		'duperlibs' => $test->path['lib'] . DS . 'super' . DS . 'duper';
	));
*/


	/**
	*	Examples for admin pages
	* 	see "Adding admin pages" documentation
	* 	--------------------------------------------------------------------------------------------------------------------
	*/
	$my_pages = array (
		'menu' => array (
			array (
				'page.title' => 'Upwardstech Google spreadsheets',
				'menu.title' => 'UT Google spreadsheets',
				'capability' => 'administrator',
				'controller' => 'main',
				'function' => 'home',
				'icon' => 'dashicons-marker',
			),
		),
	);
    $utSpreadsheet->pages($my_pages);



	/**
	*	Examples of Actions / filters
	* 	see "Adding actions/filters handlers" documentation
	* 	--------------------------------------------------------------------------------------------------------------------
	*/

	$my_actions = array (
		array(
			'tag' => 'utgetSspreadsheets',
			'controller' => 'main',
			'function' => 'get_spreadsheets',
		),
	);
    $utSpreadsheet->actions($my_actions);

	/**
	*	Examples of shortcodes
	* 	see "Adding shortcodes handlers" documentation
	* 	--------------------------------------------------------------------------------------------------------------------
	*/
    /*
	$my_shortcodes = array (
		array(
			'tag' => 'my_shortcode',   // will handle [my_shortcode]
			'controller' => 'test',
			'function' => 'shortA',
		),

	);
    $utSpreadsheet->shortcodes($my_shortcodes);
    */

    if (!wp_next_scheduled('utgetSspreadsheets')){
        wp_schedule_event(time(), 'daily', 'utgetSspreadsheets');
    }
?>
